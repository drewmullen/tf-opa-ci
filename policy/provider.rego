package main

deny[msg] {
    some key
    provider := input[key]
    is_provider_key(key)
    not provider.version
    
    msg := sprintf("Provider %s must have a version defined", [key])
}

is_provider_key(k) {
	startswith(k, "provider.")
}

deny[msg] {
    msg := "You must define a version for terraform"
    tf_block := input["terraform"]
    not tf_block.required_version
}